<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="label.site.title" /></title>
<script type="text/JavaScript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<link href="<c:url value='/static/css/bootstrap.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<header id="header">
		<jsp:include page="includes/header.jsp" />
	</header>

	<section id="sidemenu">
		<jsp:include page="includes/menu.jsp" />
	</section>

	<section id="site-content">
	
	
	
		<h2>Dynamic Dropdown Change from JSON data</h2>
		<ul id="location">
			<li><select id="country"></select></li>
			<li><select id="state"></select></li>
			<li><select id="city"></select></li>
		</ul>
		
	</section>

	<footer id="footer">
		<jsp:include page="includes/footer.jsp" />
	</footer>
</body>

 <script type="text/JavaScript">
    //get a reference to the select element
    var $ctry = $('#country');
    var $stat = $('#state');
    var $cty = $('#city');
 
    //request the JSON data and parse into the select element
    $.getJSON('dict/location/countries', function(data){
 		//clear the current content of the select
      	$ctry.html('');
      	$ctry.append('<option value="">Select</option>');
      	//iterate over the data and append a select option
      	$.each(data, function(i, obj){ 
        	$ctry.append('<option value="' + obj.abbrevation + '">' + obj.name + '</option>');
      	}) 
    });
    
    
    $ctry.change(function(){
    	var country = $("#country option:selected").val();
    	$.getJSON('dict/location/states/'+country, function(data){
    	      $stat.html('');
    	      $stat.append('<option value="">Select</option>');
    	      $.each(data, function(i, obj){ 
    	    	  $stat.append('<option value="' + obj.abbrevation + '">' + obj.name + '</option>');
    	      })
    	});
    });

    $stat.change(function(){
    	var country = $("#country option:selected").val();
    	var state = $("#state option:selected").val();
    	$.getJSON('dict/location/states/'+country+'/cities/'+state, function(data){
    	      $cty.html('');
    	      $cty.append('<option value="">Select</option>');
    	      $.each(data, function(i, obj){ 
    	    	  $cty.append('<option value="' + obj.abbrevation + '">' + obj.name + '</option>');
    	      })
    	});
    });
       
    </script>
</html>