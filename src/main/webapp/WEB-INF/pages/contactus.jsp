<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="label.site.title" /></title>
<link href="<c:url value='/static/css/bootstrap.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<header id="header">
		<jsp:include page="includes/header.jsp" />
	</header>

	<section id="sidemenu">
		<jsp:include page="includes/menu.jsp" />
	</section>

	<section id="site-content">
		<h2>Contact us page</h2>
	</section>

	<footer id="footer">
		<jsp:include page="includes/footer.jsp" />
	</footer>
</body>
</html>