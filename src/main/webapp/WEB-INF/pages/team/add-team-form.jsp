<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="label.site.title" /></title>
<link href="<c:url value='/static/css/bootstrap.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<header id="header">
		<jsp:include page="../includes/header.jsp" />
	</header>

	<section id="sidemenu">
		<jsp:include page="../includes/menu.jsp" />
	</section>

	<section id="site-content">
		<h2>Add team page</h2>
		<p>Here you can add a new team.</p>
		<form:form method="POST" commandName="team">
			<table>
				<tbody>
					<tr>
						<td><spring:message code="label.team.name" />:</td>
						<td><form:input path="name" /></td>
					</tr>
					<tr>
						<td><spring:message code="label.team.rating" />:</td>
						<td><form:input path="rating" /></td>
					</tr>
					
					<tr><td>
					<form:checkboxes items="${data}" path="selections"/>
					</td></tr>
					<tr>
						<td><input type="submit" value="<spring:message code="btn.team.add" />" /></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</form:form>
	</section>

	<footer id="footer">
		<jsp:include page="../includes/footer.jsp" />
	</footer>
</body>
</html>