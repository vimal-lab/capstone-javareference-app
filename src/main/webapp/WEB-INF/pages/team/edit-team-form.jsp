<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="label.site.title" /></title>
<link href="<c:url value='/static/css/bootstrap.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<header id="header">
		<jsp:include page="../includes/header.jsp" />
	</header>

	<section id="sidemenu">
		<jsp:include page="../includes/menu.jsp" />
	</section>

	<section id="site-content">
		<h2>Edit team page</h2>
		<p>Here you can edit the existing team.</p>
		<p>${message}</p>
		<form:form method="POST" commandName="team">
			<table>
				<tbody>
					<tr>
						<td>Name:</td>
						<td><form:input path="name" /></td>
					</tr>
					<tr>
						<td>Rating:</td>
						<td><form:input path="rating" /></td>
					</tr>
					<tr>
						<td><input type="submit" value="Edit" /></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</form:form>

		<p>
			<a href="${pageContext.request.contextPath}/index.html">Home page</a>
		</p>
	</section>

	<footer id="footer">
		<jsp:include page="../includes/footer.jsp" />
	</footer>
</body>
</html>

