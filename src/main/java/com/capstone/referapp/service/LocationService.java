package com.capstone.referapp.service;

import java.util.List;

import com.capstone.referapp.util.model.City;
import com.capstone.referapp.util.model.Country;
import com.capstone.referapp.util.model.State;

public interface LocationService {

	List<Country> getCountries();
	
	List<State> getStates(String country);

	List<City> getCities(String country, String state);

}
