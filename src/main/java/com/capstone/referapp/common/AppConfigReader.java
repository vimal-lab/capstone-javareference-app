package com.capstone.referapp.common;

import javax.annotation.Resource;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:app-db-orm.properties")
@PropertySource("classpath:app-custom.properties")
public class AppConfigReader {

	@Resource
	private Environment env;
	
	public final String getConfig(String key) {
		return env.getRequiredProperty(key);
	}
}
