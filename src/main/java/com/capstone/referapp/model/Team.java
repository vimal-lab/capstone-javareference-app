package com.capstone.referapp.model;

import java.beans.Transient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="teams")
public class Team {
	
	@Id
	@GeneratedValue
	@Column(name="ID")
	private Integer teamId;
	
	private String name;
	
	private Integer rating;
	private String[] selections;
	
	public Integer getId() {
		return teamId;
	}
	public void setId(Integer teamId) {
		this.teamId = teamId;
	}
	@Transient
	public String[] getSelections() {
		return selections;
	}
	public void setSelections(String[] selections) {
		this.selections = selections;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}

}
