package com.capstone.referapp.util.model;

import java.util.ArrayList;
import java.util.List;

public class Country {

	private String name;
	private String abbrevation;
	private String[] geoLocation;
	private List<State> states;

	public Country(String name, String abbrevation) {
		super();
		this.name = name;
		this.abbrevation = abbrevation;
	}

	public List<State> getStates() {
		return states;
	}

	public void setStates(List<State> states) {
		this.states = states;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbbrevation() {
		return abbrevation;
	}

	public void setAbbrevation(String abbrevation) {
		this.abbrevation = abbrevation;
	}

	public String[] getGeoLocation() {
		return geoLocation;
	}

	public void setGeoLocation(String[] geoLocation) {
		this.geoLocation = geoLocation;
	}

	public void addState(State st) {
		if(this.states == null) {
			this.states = new ArrayList<State>();
		}
		this.states.add(st);
	}
}
