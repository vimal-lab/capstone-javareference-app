package com.capstone.referapp.util.model;

import java.util.Map;

public class City {

	private String name;
	private String abbrevation;
	private Map<String, String> postalCodes;
	private String[] geoLocation;

	public City(String name, String abbr) {
		super();
		this.name = name;
		this.abbrevation = abbr;
	}

	public String getAbbrevation() {
		return abbrevation;
	}

	public void setAbbrevation(String abbrevation) {
		this.abbrevation = abbrevation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, String> getPostalCodes() {
		return postalCodes;
	}

	public void setPostalCodes(Map<String, String> postalCodes) {
		this.postalCodes = postalCodes;
	}

	public String[] getGeoLocation() {
		return geoLocation;
	}

	public void setGeoLocation(String[] geoLocation) {
		this.geoLocation = geoLocation;
	}

}
